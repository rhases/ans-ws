var plans = db.healthplans_nacional_liberado.find();

plans.forEach(function (plan){
  
  var produtos_hospitais = db.ans_produtos_hospitais.find({ $and: [ { "REG_PRODUTO": NumberInt(plan._id.cod) }, { "REG_OPERADORA": plan._id.operator } ] })
  
  plan.healthProviders = produtos_hospitais.map(function(prod_hosp) {
    
      var healthProvider = {"provider" : NumberInt(prod_hosp.CNES_PRESTADOR)};
      healthProvider.services = [];
      if(prod_hosp.PRONTO_SOCORRO && prod_hosp.PRONTO_SOCORRO.toUpperCase() == "SIM") {
	    healthProvider.services.push("pronto-socorro");
	  }
	  return healthProvider;
	});
	
	db.healthplans_and_providers.update({"_id" : plan._id}, plan, true);
});