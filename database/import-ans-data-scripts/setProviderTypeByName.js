db.providers_test.updateMany({}, {$set: {"type" : "clinic"}});
db.providers_test.updateMany({ $or: [ { "name": { $regex: /.*\QHOSPITAL\E.*/, $options: 'i' } }, { "legalName": { $regex: /.*\QHOSPITAL\E.*/, $options: 'i' } } ] }, {$set: {"type" : "hospital"}});
db.providers_test.updateMany({ $or: [ { "name": { $regex: /.*\QLABORAT\E.*/, $options: 'i' } }, { "legalName": { $regex: /.*\QLABORAT\E.*/, $options: 'i' } } ] }, {$set: {"type" : "laboratory"}});
